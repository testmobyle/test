local composer = require("composer")
local scene = composer.newScene()


local function networkListener( event )
    if ( event.isError ) then
        print( "Network error - download failed: ", event.response )
    end
end


local function ParseJsonFile(filename)
    local json = require( "json" )
    local path = system.pathForFile(filename, system.ResourceDirectory )
    local decoded, pos, msg = json.decodeFile( path )
    if not decoded then
        return "Error: Decode failed at "
    end
    return decoded.images;
end


function scene:create( event )

    local sceneGroup = self.view
    local mytext = display.newText({text = "загрузка", x = display.contentCenterX, y = 53, font = native.systemFont, fontSize = 50,});
    sceneGroup:insert(mytext);
    images = ParseJsonFile("images.json")

    for i=1, #images do
        network.download(images[i], "GET", networkListener, params, "image" .. i ..".png", system.TemporaryDirectory)
    end

   composer.removeScene( "DownloadFiles" )
end

function scene:destroy( event )

    local sceneGroup = self.view
    composer.gotoScene("mywidget")
end

scene:addEventListener("create", scene);
scene:addEventListener("destroy", scene);
return scene
