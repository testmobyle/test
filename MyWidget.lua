local widget = require( "widget" )
local composer = require("composer")
local scene = composer.newScene()

local imgcount = 0
local curimg = 0
local direction = ""
local function scrollListener( event )
    local phase = event.phase

	if phase == "ended" and direction ~= nil  then 
    	moveto(direction)
    end
    direction = event.direction
    if event.direction == nil then
    else
		direction = event.direction
	end
	return true
end

local scrollView = widget.newScrollView(
    {
        top = 0,
        left = 0,
        width = _W,
        height = _H,
        scrollWidth = _W,
        scrollHeight = _H,
        verticalScrollDisabled = true,
        listener = scrollListener
    }
	)

function moveto(d)
	local  m = 1
	if direction == "left" then
		m=-1
	else
		m=1
	end

	if direction == "left" and curimg < imgcount-1 then
		curimg = curimg + 1
	end
	if direction ~= "left" and curimg > 0 then
		curimg = curimg - 1
	end	
	scrollView:scrollToPosition
	{
    	x = - _W * curimg,
    	time = 400
	}
end

function scene:create(event)
   local sceneGroup = self.view;
   sceneGroup:insert(scrollView)
end

for i=1, #images do
	local sky = display.newImageRect("image"..i..".png", system.TemporaryDirectory,  _W, _H )
	sky.x = centerX + (_W * (i -1))
	sky.y = centerY
	scrollView:insert(sky)
	imgcount = imgcount + 1
end


scene:addEventListener("create", scene)
return scene